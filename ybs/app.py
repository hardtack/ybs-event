import views
import random
import importlib
import collections
import forms as f
import models as m
import formencode_jinja2
from auth import auth_blueprint, get_login_user
from gettext import gettext as _
from flask import Flask, g
from errors import ResponseError, FormError
from render import render, convert_encoder
from middlewares import MethodParameterMiddleware
from formencode.htmlgen import html
from contrib.utils.proxy import Proxy
from contrib.ext.sqlalchemy_ext import create_engine
from contrib.middlewares.sqlalchemymiddleware import SQLAlchemyMiddleware
from sqlalchemy_imageattach import context
from sqlalchemy_imageattach.stores import fs


class App(Flask):
    """Base Flask app for Epicl API
    """
    def handle_exception(self, e):
        if isinstance(e, ResponseError):
            if isinstance(e, FormError):
                data = e.form
            else:
                data = e
            return render(data, status=e.status_code, template=e.template,
                ctx=e.ctx)
        else:
            return super(App, self).handle_exception(e)

    def apply_task(self, task, *args, **kwargs):
        """Applies task with config option.  Depends on configuration, It can be
        run locally or asynchronously.  
        """
        if self.config.get('LOCAL_TASK', False):
            return task.apply(*args, **kwargs)
        else:
            return task.apply_async(*args, **kwargs)

def epicl_error_formatter(error):
    return (unicode(html.br())
        + unicode(html.span(error, **{'class':'error-message'})))

self = importlib.import_module(__name__)

def create_app(config):
    """Creates flask application with configuration
    """
    app = App(__name__)

    # Configure application
    if isinstance(config, collections.Mapping):
        app.config.update(config)
    else:
        app.config.from_object(config)

    # View registering
    views.register_views(app)

    # DB connection
    app.db_engine = create_engine(app.config['DATABASE'])
    m.Base.metadata.bind = app.db_engine
    m.Session.configure(bind=app.db_engine)

    # Middlewares
    SQLAlchemyMiddleware(m.Session, app)

    #Store
    app.store = fs.HttpExposedFileSystemStore('images', 'images/')
    app.wsgi_app = app.store.wsgi_middleware(app.wsgi_app)

    @app.before_request
    def store_before_request():
        context.push_store_context(app.store)

    @app.teardown_request
    def store_teardown_request(exception=None):
        context.pop_store_context()


    # Auth
    app.register_blueprint(auth_blueprint)

    @app.before_request
    def before_request():
        g.user = get_login_user()

    # Jinja Configutations
    @app.context_processor
    def inject_globals():
        return {
            'm':m,
            'f':f,
            'is_loggedin':(lambda:not g.user is None),
            '_':_,
            'tojson':convert_encoder.encode,
            'random':random.choice,
        }
    app.jinja_env.add_extension(formencode_jinja2.formfill)
    app.jinja_env.formfill_config = {
        'auto_error_formatter':epicl_error_formatter,
        'prefix_error':False,
    }
    app.jinja_env.formfill_error_formatters['default'] = epicl_error_formatter

    # Middlewares
    app.wsgi_app = MethodParameterMiddleware(app.wsgi_app)

    return app

