""":mod:`epicl.contrib.utils.form`
==================================

Provides utils for :mod:`formencode` and :mod:`epicl.forms`
"""

import formencode

def set_form_error(form, msg):
    """Set form-level error.  

    :param form: target form
    :param msg: error msg.  
    """
    e = formencode.Invalid(msg, form.form, None)
    form.error = e

def set_error(form, fieldname, msg):
    """Set error to field in `form`.  

    :param form: target form
    :param fieldname: target field name
    :param msg: error msg.  
    """
    value = getattr(getattr(form, fieldname, None), 'value', None)

    e = formencode.Invalid(msg, value, None)
    form.error = formencode.Invalid(None, None, None, error_dict={fieldname:e})
