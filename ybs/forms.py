# -*- encoding: utf-8 -*-
""":mod:`forms` --- Forms
~~~~~~~~~~~~~~~~~~~~~~~~~

"""
import json
import numbers
import datetime
import werkzeug
import formencode
import models as m
from flask import request
from gettext import gettext as _
from formencode import validators as v
from errors import FormError
from contrib.utils import timestamp
from sqlalchemy.sql import exists

class JSON(formencode.FancyValidator):
    """JSON type validator
    """
    messages = {
        'cannot_convert':_('This value cannot be converted to json object'),
    }
    def _convert_to_python(self, value, state):
        if value is None:
            return None
        try:
            value = json.loads(value)
        except:
            raise formencode.Invalid(self.messages['cannot_convert'],
                value, state)
        return value

    def _convert_from_python(self, value, state):
        if value is None:
            return None
        return json.dumps(value)

class Timestamp(formencode.FancyValidator):
    """Timestamp validator
    """
    messages = {
        "not_number":_('Given value is not real number'),
    }
    def _convert_to_python(self, value, state):
        if value is None:
            return None
        if not isinstance(value, numbers.Real):
            raise formencode.Invalid(self.messages['not_number'], value, state)
        return datetime.datetime.fromtimestamp(value)

    def _convert_from_python(self, value, state):
        if value is None:
            return None
        return timestamp(value)

class File(formencode.FancyValidator):
    def __init__(self, *args, **kwargs):
        super(File, self).__init__(accept_iterator=True, *args, **kwargs)

    def _to_python(self, value, state):
        if getattr(self, 'not_empty', False) and value is None:
            raise formencode.Invalid(_('Please upload a file'), value, state)

        if not isinstance(value, werkzeug.FileStorage):
            raise formencode.Invalid(_('It\'s not a file'), value, state)

        return value

class UniqueSQLAlchemyField(formencode.FancyValidator):
    def __init__(self, field, fieldname, *args, **kwargs):
        super(UniqueSQLAlchemyField, self).__init__(*args, **kwargs)
        self.field = field
        self.fieldname = fieldname

    def _to_python(self, value, state):
        session = m.best_session()
        e = session.query(
            exists().where(
                self.field==value
            )
        ).scalar()
        if e:
            msg = _('%(fieldname)s "%(value)s" already exists.') % {
                'fieldname':self.fieldname,
                'value':value,
            }
            raise v.Invalid(msg, value, state)
        return value

class CommaSeparated(formencode.FancyValidator):
    messages = {
        'empty':_('an element of the list is empty')
    }
    def _to_python(self, value, state):
        return value.split(",")

    def validate_python(self, value, state):
        for elem in value:
            if not elem: 
                raise formencode.Invalid(self.messages['empty'], value, state) 


class Form(formencode.Schema):
    def __init__(self, *args, **kwargs):
        super(Form, self).__init__(*args, **kwargs)
        self.form = {}
        self.error = None
        self.data = {}

    def _value_is_iterator(self, value):
        if isinstance(value, werkzeug.FileStorage):
            # Protect stream
            return False
        return super(Form, self)._value_is_iterator(value)

    @property
    def error_dict(self):
        if (self.error is None
            or not self.error.error_dict
            ):
            return {}
        return self.error.error_dict

    def validate(self):
        try:
            self.data = self.to_python(self.form)
        except formencode.Invalid, e:
            self.error = e
            return False
        return True

class YBSForm(Form):
    def _merge_files_and_form(self, form, files):
        merged = {}
        for k, v in form.iteritems():
            merged[k] = v
        for k, v in files.iteritems():
            merged[k] = v
        return merged

    def validate(self, template=None, ctx=None):
        self.form = self._merge_files_and_form(request.form, request.files)
        if not request.method.upper() in ['POST', 'PUT']:
            return False
        if super(YBSForm, self).validate():
            return True
        raise FormError(self, template=template, ctx=ctx)

class LoginForm(YBSForm):
    """로그인을 위한 폼. 

    *   username

    *   password
        패스워드

    """
    username = v.PlainText(not_empty=True, strip=True)
    password = v.PlainText(not_empty=True)

class CardForm(YBSForm):
    """카드 만드는 폼.  

    *   message
        메시지

    *   name
        이름

    *   photo
        사진 
    """
    name = v.UnicodeString(not_empty=True, max_len=20)
    message = v.UnicodeString(not_empty=True)
    photo = File(not_empty=True)
