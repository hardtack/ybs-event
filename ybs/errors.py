class ResponseError(Exception):
    def __init__(self, message, status_code, template=None, headers=None,
            ctx=None):
        super(ResponseError, self).__init__()
        self.message = message
        self.status_code = status_code
        self.template = template
        self.headers = {} if headers is None else headers
        self.ctx = {} if ctx is None else ctx

class FormError(ResponseError):
    def __init__(self, form, status_code=400, template=None, headers=None,
            ctx=None):
        message = unicode(form.error)
        super(FormError, self).__init__(
            message, 400, template=template, headers=headers, ctx=ctx
        )
        self.form = form

    def __repr__(self):
        return (u'<FormError(%(form)s, %(status_code)s, %(template)s, '
                '%(headers)s)') % {
                    'form':self.form,
                    'status_code':self.status_code,
                    'template':self.template,
                    'headers':self.headers,
                }

    def __unicode__(self):
        return unicode(self.message)

    def __str__(self):
        return unicode(self).encode('utf-8')

