from cgi import parse_qs

class MethodParameterMiddleware(object):
    """Since HTML supports only :http:method:`get` and :http:method:`post` , 
    We cannot make RESTful API.  But we can parameterize http method by URL 
    query paramenter.  

    If ``__method__`` in query arguments, then we will use it as HTTP method.  
    """
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        parameters = parse_qs(environ.get('QUERY_STRING', ''))
        if '__method__' in parameters:
            method = parameters['__method__'][0].upper()
            environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)

