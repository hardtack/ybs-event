import code
import config
import ybs.models as m
import ybs.forms as f
from flask import g
from ybs.app import create_app
from sqlalchemy_imageattach.context import store_context

def shell():
    app = create_app(config)
 
    session = m.Session()
    env = {
        'app':app,
        'models':m,
        'm':m,
        'forms':f,
        'f':f,
        'session':session,
        'g':g,
    }
    with app.app_context():
        with store_context(app.store):
            code.interact(local=env)

def main():
    shell()

if __name__ == '__main__':
    main()
